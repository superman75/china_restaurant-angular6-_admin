import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-orderlist',
  templateUrl: './orderlist.component.html',
  styleUrls: ['./orderlist.component.scss']
})
export class OrderlistComponent implements OnInit {

  filterDate = new Date().toISOString().split('T')[0];
  panelOpenState = false;
  customers = [
    {
      name : 'XXX',
      phone : '123456789',
      orders : [
        {
          image_url : 'assets/imgs/1.png',
          food_name : 'biscake',
          price : 20,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Ago',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/2.png',
          food_name : 'Suger',
          price : 10,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Later',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/3.png',
          food_name : 'sandwich',
          price : 5,
          count : 4,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Today',
          state : 'delivery'
        }
      ]
    },
    {
      name : 'YYY',
      phone : '132412354',
      orders : [
        {
          image_url : 'assets/imgs/1.png',
          food_name : 'biscake',
          price : 20,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Ago',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/2.png',
          food_name : 'Suger',
          price : 10,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Later',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/3.png',
          food_name : 'sandwich',
          price : 5,
          count : 4,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Today',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/1.png',
          food_name : 'biscake',
          price : 20,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Ago',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/2.png',
          food_name : 'Suger',
          price : 10,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Later',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/3.png',
          food_name : 'sandwich',
          price : 5,
          count : 4,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Today',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/1.png',
          food_name : 'biscake',
          price : 20,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Ago',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/2.png',
          food_name : 'Suger',
          price : 10,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Later',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/3.png',
          food_name : 'sandwich',
          price : 5,
          count : 4,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Today',
          state : 'delivery'
        }
      ]
    },
    {
      name : 'ZZZ',
      phone : '124352342',
      orders : [
        {
          image_url : 'assets/imgs/1.png',
          food_name : 'biscake',
          price : 20,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Ago',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/2.png',
          food_name : 'Suger',
          price : 10,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Later',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/3.png',
          food_name : 'sandwich',
          price : 5,
          count : 4,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Today',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/1.png',
          food_name : 'biscake',
          price : 20,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Ago',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/2.png',
          food_name : 'Suger',
          price : 10,
          count : 2,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Later',
          state : 'delivery'
        },
        {
          image_url : 'assets/imgs/3.png',
          food_name : 'sandwich',
          price : 5,
          count : 4,
          reserve_time : '9/1/2018 5:30PM',
          when : 'Today',
          state : 'delivery'
        }
      ]
    },
    {
      name : 'TTT',
      phone : '125643234',
      orders : []
    },
    {
      name : 'CCC',
      phone : '123456789',
      orders : []
    },
    {
      name : 'XXX',
      phone : '125324346',
      orders : []
    },
  ];
  constructor(private _router: Router) { }

  ngOnInit() {
  }

}
