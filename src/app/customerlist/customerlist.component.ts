import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customerlist',
  templateUrl: './customerlist.component.html',
  styleUrls: ['./customerlist.component.scss']
})
export class CustomerlistComponent implements OnInit {

  customers = [
    {
      name : 'XXX',
      phone : '123456789'
    },
    {
      name : 'YYY',
      phone : '132412354'
    },
    {
      name : 'ZZZ',
      phone : '124352342'
    },
    {
      name : 'TTT',
      phone : '125643234'
    },
    {
      name : 'CCC',
      phone : '123456789'
    },
    {
      name : 'XXX',
      phone : '125324346'
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
