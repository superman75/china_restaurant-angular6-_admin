import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule, MatSelectModule,
  MatFormFieldModule, MatInputModule, MatButtonToggleModule, MatDatepickerModule,
  MatRadioModule, MatExpansionModule, MatBadgeModule
} from '@angular/material';
import { AgmCoreModule} from '@agm/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { OrderlistComponent } from './orderlist/orderlist.component';
import { FooddetailComponent } from './fooddetail/fooddetail.component';
import {FormsModule} from "@angular/forms";
import { LoginComponent } from './login/login.component';
import { ForgotComponent } from './forgot/forgot.component';
import { CustomerlistComponent } from './customerlist/customerlist.component';
import { AddnewfoodComponent } from './addnewfood/addnewfood.component';
import { ChangeComponent } from './change/change.component';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MainNavComponent,
    OrderlistComponent,
    FooddetailComponent,
    LoginComponent,
    ForgotComponent,
    CustomerlistComponent,
    AddnewfoodComponent,
    ChangeComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatListModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    MatRadioModule,
    MatListModule,
    MatExpansionModule,
    MatBadgeModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCm7rSYPVVqY0W2TAO0d8h8J_PYmH8cKqw'
    }),
    MatFormFieldModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
