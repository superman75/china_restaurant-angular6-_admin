import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-fooddetail',
  templateUrl: './fooddetail.component.html',
  styleUrls: ['./fooddetail.component.scss']
})
export class FooddetailComponent implements OnInit {

  order = {
    image_url : 'assets/imgs/5.png',
    food_name : 'Black bread',
    food_desc : 'dummy dummy',
    price : 10,
    favor : 10,
    order_date : '09/01/2018 8:30AM',
    count : 1,
    state : "dis count"
  };
  constructor(private _router: Router) { }

  ngOnInit() {
  }


  Save() {
    console.log('Save Food Detail');
    this._router.navigate(['home']);
  }
}
