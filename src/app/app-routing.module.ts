import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {OrderlistComponent} from './orderlist/orderlist.component';
import {FooddetailComponent} from "./fooddetail/fooddetail.component";
import {CustomerlistComponent} from "./customerlist/customerlist.component";
import {LoginComponent} from "./login/login.component";
import {ProfileComponent} from "./profile/profile.component";
import {ForgotComponent} from "./forgot/forgot.component";
import {ChangeComponent} from "./change/change.component";
import {AddnewfoodComponent} from "./addnewfood/addnewfood.component";

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'orderlist',
    component: OrderlistComponent
  },
  {
    path: 'fooddetail',
    component: FooddetailComponent
  },
  {
    path: 'customerlist',
    component: CustomerlistComponent
  },
  {
    path: 'addnewfood',
    component: AddnewfoodComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'forgot',
    component: ForgotComponent
  },
  {
    path: 'change',
    component: ChangeComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
