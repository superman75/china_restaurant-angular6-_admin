import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addnewfood',
  templateUrl: './addnewfood.component.html',
  styleUrls: ['./addnewfood.component.scss']
})
export class AddnewfoodComponent implements OnInit {

  food = {
    image_url : '',
    name : '',
    price : '',
    desc : '',
    state : 'new',
    when : 'all'
  };
  constructor() { }

  ngOnInit() {
  }

}
