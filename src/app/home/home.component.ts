import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  filters: any[] = [
    {
      value: 1,
      viewValue: 'All Food'
    },
    {
      value: 2,
      viewValue: 'Breakfast'
    },
    {
      value: 3,
      viewValue: 'Lunch'
    },
    {
      value: 4,
      viewValue: 'Dinner'
    }
  ];
  selected = 1;
  foods: any[] = [
    {
      image_url : 'assets/imgs/1.png',
      food_name : 'biscake',
      food_desc : 'dummy dummy',
      price : 10,
      favor : 10,
      state : 'new',
      detail : 'breakfast'
    },
    {
      image_url : 'assets/imgs/2.png',
      food_name : 'sugar',
      food_desc : 'dummy dummy',
      price : 10,
      favor : 10,
      state : 'new',
      detail : 'breakfast'
    },
    {
      image_url : 'assets/imgs/3.png',
      food_name : 'pizza',
      food_desc : 'dummy dummy',
      price : 10,
      favor : 10,
      state : 'new',
      detail : 'breakfast'
    },
    {
      image_url : 'assets/imgs/4.png',
      food_name : 'sandwich',
      food_desc : 'dummy dummy',
      price : 10,
      favor : 10,
      state : 'dis count',
      detail : 'lunch'
    },
    {
      image_url : 'assets/imgs/5.png',
      food_name : 'bread',
      food_desc : 'dummy dummy',
      price : 10,
      favor : 10,
      state : 'dis count',
      detail : 'dinner'
    }
  ];
  constructor(private _router: Router) { }

  ngOnInit() {
  }

  gotoDetail(i) {
    this._router.navigate(['fooddetail']);
  }
}
